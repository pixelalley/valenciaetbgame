﻿using UnityEngine;
using System.Collections;

public class agentscript : MonoBehaviour {

	// Use this for initialization
	public Transform target;
	Transform initialposition;

	NavMeshAgent agent;


	void Start () {
		if (target == null) 
		{
			if (gameObject.tag == "car") {
				target = GameObject.Find ("target").transform;
			} else {
				target = GameObject.Find ("targetpeople").transform;
			}
		}

		initialposition = transform;
		agent = GetComponent<NavMeshAgent> ();
	}

	public void restartposition()
	{
		transform.position = initialposition.position;
		transform.rotation = initialposition.rotation;
		transform.localScale = initialposition.localScale;
	}


	// Update is called once per frame
	void Update () {
		agent.SetDestination (target.position);
	}
	void FixedUpdate()
	{
		float distance = Vector3.Distance (target.position, transform.position);
		if (distance < 20) 
		{
			Destroy (gameObject);
		}

//		transform.GetChild (0).transform.rotation = Quaternion.Euler(transform.rotation.x,transform.rotation.y, 0);
//		if (transform.GetChild (0).transform.rotation.z != 0) {
//			transform.GetChild (0).transform.LookAt (target);
//		}
	}
}
