﻿using UnityEngine;
using System.Collections;

public class spawagents : MonoBehaviour {

	float timelimit=4;
	float timeruning;

	public GameObject[] agents;
	public Transform spawnpos;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		timeruning += Time.deltaTime;
		if (timeruning > timelimit) {
			timeruning = 0;
			int random = Random.Range (0, agents.Length);
			GameObject clone = Instantiate (agents [random], spawnpos.position, Quaternion.identity,spawnpos.parent) as GameObject;
			clone.name = agents [random].name;
		}
	
	}
}
