﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed=0;

	public GameObject maingame;
	public GameObject principalmenu;
	public GameObject iniciojuego;
	public GameObject delitocanvas;
	public GameObject wincanvas;
	public GameObject temporalcamera;

	public GameObject tacometro;

	public Vector3 startposition;

	// Use this for initialization
	void Start () {
		startposition = transform.position;
	}
	
	// Update is called once per frame

	public void acceleration()
	{
		speed += Time.deltaTime*7*1;
		if (speed > 150) {
			speed = 150;
		}
		print ("helo");
	}
	public void deceleration()
	{
		speed += Time.deltaTime*7*-1;
		if (speed >= 0) {
			speed -= Time.deltaTime * 20;
		}
		print ("helo");
	}


	void Update () {

		#if UNITY_STANDALONE
		if (Input.GetAxis ("Vertical")!=0) {
			speed += Time.deltaTime*7* Input.GetAxis("Vertical");
			if (speed > 150) {
				speed = 150;
			}
		} else 
		{
			if (speed >= 0) {
				speed -= Time.deltaTime * 20;
			}
		}
		transform.Rotate (Vector3.up, speed * Time.deltaTime * Input.GetAxis ("Horizontal"));
		#else
		transform.Rotate (Vector3.up, speed * Time.deltaTime * Input.acceleration.x);
		#endif



		tacometro.transform.localRotation = Quaternion.Euler (0, 180, speed*1.2f);
		transform.Translate (0, 0, (speed/5) * Time.deltaTime);
	}
	void OnCollisionEnter(Collision coll)
	{
		if (coll.collider.name == "deadzone") {
			enviarainicialpos ();
			Time.timeScale = 0;
			speed = 0;
			wincanvas.SetActive (true);
		}
		if (coll.collider.name == "levelcoliders" || coll.collider.name == "smartobstacles" ) {
			speed -= Time.deltaTime*20;
		}

		if (coll.collider.name.Substring (0, "agent".Length) == "agent") 
		{

			Time.timeScale = 0;
			speed = 0;
			Destroy (coll.collider.gameObject,1);
			coll.collider.enabled = false;
			delitocanvas.SetActive (true);
		}
		print (coll.collider.name);
	}

	public void enviarainicialpos()
	{
		transform.position = startposition;
		transform.rotation =Quaternion.identity;
	}

	public void reiniciar()
	{
		Time.timeScale = 1;
		enviarainicialpos ();
	}
	public void timescalenormal()
	{
		Time.timeScale = 1;
	}

	public void menuprincipal()
	{
		Time.timeScale = 1;
		enviarainicialpos ();
		principalmenu.SetActive (true);
		iniciojuego.SetActive (false);
		temporalcamera.SetActive (true);
		maingame.SetActive (false);
	}

}
